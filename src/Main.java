import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main {
    // Sensible defaults.
    private static final String DEFAULT_PORT = "COM1";
    private static final String DEFAULT_FILENAME = "snuffelneus_log.txt";
    private static final Boolean DEFAULT_POLLING = false;
    private static final int DEFAULT_INTERVAL_MS = 2000;

    private String fileName;
    private String comPort;
    private Boolean usePolling;
    private int interval;

    private SerialManager serialManager;

    /**
     * Entry point.
     * @param args Arguments for the application.
     */
    public static void main(String[] args) {
        String comPort = DEFAULT_PORT;
        String logFileName = DEFAULT_FILENAME;
        Boolean usePolling = DEFAULT_POLLING;
        int interval = DEFAULT_INTERVAL_MS;

        if(args.length > 0) {
            int i = 0;
            for(; i < args.length; i++) {
                if(args[i].equals("--file") || args[i].equals("-f")) {
                    logFileName = args[i + 1];
                    i++;

                    System.out.println("Selected filename: " + logFileName);
                }

                if(args[i].equals("--port") || args[i].equals("-p")) {
                    comPort = args[i + 1];
                    i++;

                    System.out.println("Selected COM port: " + comPort);
                }

                if(args[i].equals("--interval") || args[i].equals("-i")) {
                    comPort = args[i + 1];
                    i++;

                    System.out.println("Selected interval: " + interval);
                }

                if(args[i].equals("--poll-device") || args[i].equals("-pl")) {
                    usePolling = true;
                    System.out.println("Using polling");
                }
            }
        }
        else {
            // Maybe handle this in a 'cleaner' manner?
            // Defaults seem sensible and fine BUT using a wrong COM port may upset connected devices.
            System.out.println("Select filename and port using: ");
            System.out.println("--file/-f [filename], e.g. \"-f snuffelneus_log.txt\"");
            System.out.println("--port/-p [port], e.g. \"-p COM1\"");
            System.out.println("--poll-device/-pl");
            System.out.println("--interval/-i [milliseconds], e.g. \"-i 2000\"");
            System.out.println("Using defaults.");
        }

        System.out.println("Settings: " + DEFAULT_PORT + " - " + DEFAULT_FILENAME + " - " + DEFAULT_INTERVAL_MS + "ms - polling: " + DEFAULT_POLLING);
        new Main(logFileName, comPort, usePolling, interval);
    }

    /**
     * Constructor, initializes all used variables.
     * @param logFileName
     * @param comPort
     * @param usePolling
     */
    public Main(String logFileName, String comPort, Boolean usePolling, int interval) {
        this.fileName = logFileName;
        this.comPort = comPort;
        this.usePolling = usePolling;
        this.interval = interval;

        serialManager = new SerialManager(comPort, 9600);

        System.out.println("Using following Serial settings:");
        System.out.println(serialManager.getPortSettingsString());

        loopForInput(usePolling, interval);
    }

    /**
     * Method that handles the acquirement loop.
     * @param usePolling Toggle polling, this sends the measurement request to the Snuffelneus device.
     */
    private void loopForInput(Boolean usePolling, int interval) {
        while(true) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(usePolling) {
                serialManager.writeString("wakeup/n");
            }

            String readData = serialManager.blockingRead();
            appendToLogFile(fileName, readData);
        }
    }

    /**
     * Method to append a line to a text file.
     * WARNING: Pretty destructive method, will append to pretty much any file that can be opened in 'text' mode.
     * @param fileName Name for the log file.
     * @param input Input to write to the log file.
     */
    private void appendToLogFile(String fileName, String input) {
        BufferedWriter writer = null;

        try {
            String timeLog = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
            File logFile = new File(fileName);

            writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.write(timeLog + " " + input);
            System.out.print(timeLog + " " + input);
        } catch (Exception e) {
            System.out.println("Unable to write to log file.");
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                System.out.println("Unable to close output writer.");
                e.printStackTrace();
            }
        }
    }
}
