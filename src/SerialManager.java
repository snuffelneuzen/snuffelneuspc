import java.io.*;

import gnu.io.*;

/**
 * Class responsible for all things serial.
 * Uses the RxTx library, which needs dll/so files accordingly.
 */
public class SerialManager
{
    private String comPort = "COM1";

    private CommPortIdentifier portIdentifier;
    private static SerialPort serialPort;
    private OutputStream outputStream;
    private InputStream inputStream;

    /**
     * Default constructor, without a baudrate.
     * @param comPort COM port for which the connection should be managed.
     */
    public SerialManager(String comPort) {
        this(comPort, 0);
    }

    /**
     * Constructor with baudrate overload.
     * @param comPort COM port for which the connection should be managed.
     * @param baudRate Baudrate that should be set for the device.
     */
    public SerialManager(String comPort, int baudRate)
    {
        this.comPort = comPort;
        initComPort();
        setBaudRate(baudRate);
    }

    /**
     * Handles the initialisation of the COM port for this Serial Manager instance.
     */
    private void initComPort()
    {
        try {
            portIdentifier = CommPortIdentifier.getPortIdentifier(comPort);
        } catch (NoSuchPortException ex) {
            System.out.println("No such port: " + comPort);
            ex.printStackTrace();
            System.exit(-1);
        }

        try {
            serialPort = (SerialPort) portIdentifier.open("SnuffelneusPC", 2000);
        } catch (PortInUseException ex) {
            System.out.println("Port in use: " + comPort);
            ex.printStackTrace();
            System.exit(-1);
        }

        try {
            outputStream = serialPort.getOutputStream();
            inputStream = serialPort.getInputStream();
        } catch (IOException ex) {
            System.out.println("Unable to open output and input streams.");
            ex.printStackTrace();
            System.exit(-1);
        }

        try {
            serialPort.setSerialPortParams(9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
        } catch (UnsupportedCommOperationException ex) {
            System.out.println("Unable to set serial port parameters.");
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Perform a BLOCKING read on the Serial port. Waits until all data has been read.
     * Input buffer CAN overflow, so if large amounts of data is being read; update.
     * @return The read data from the device as String.
     */
    public String blockingRead() {
        byte[] buffer = new byte[1024];
        int readOffset = 0;
        int totalMessageLength = 0;
        String output = "";

        try {
            while (inputStream.available() > 0) {
                byte[] inputBuffer = new byte[256];
                int bytes = inputStream.read(inputBuffer);

                System.arraycopy(inputBuffer, 0, buffer, readOffset, bytes);
                readOffset += bytes;
                totalMessageLength += bytes;

                output = new String(buffer, 0, totalMessageLength);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output;
    }

    /**
     * Method for writing a character to the serial port.
     * @param character Input that should be written to the serial port.
     */
    public void writeChar(char character) {
        try
        {
            outputStream.write(character);
        } catch (IOException ex) {
            System.out.println("Unable to write character to serial.");
            ex.printStackTrace();
        }
    }

    /**
     * Method for writing a string to the serial port.
     * @param inputString Input that should be written to the serial port.
     */
    public void writeString(String inputString) {
        try
        {
            outputStream.write(inputString.getBytes());
        } catch (IOException ex) {
            System.out.println("Unable to write string to serial.");
            ex.printStackTrace();
        }
    }

    /**
     * Handles 'building' the port settings as a string, and returns it.
     * @return The current serial port settings used.
     */
    public String getPortSettingsString() {
        return "COM Port: " + comPort + "\n" +
                "Baudrate: " + serialPort.getBaudRate() + "bps \n" +
                "Databits: " + serialPort.getDataBits() + "\n" +
                "Stopbits: " + serialPort.getStopBits();
    }

    /**
     * Handles setting the baud rate to the serial port.
     * @param baudRate The baud rate that should be used for the device.
     */
    public void setBaudRate(int baudRate) {
        int dataBits = serialPort.getDataBits();
        int stopBits = serialPort.getStopBits();
        int parity = serialPort.getParity();

        try
        {
            serialPort.setSerialPortParams(baudRate,dataBits, stopBits, parity);
        } catch (UnsupportedCommOperationException ex)
        {
            System.out.println("Unable to set serial port parameters (wrong baudrate?)");
            ex.printStackTrace();
        }
    }
}
 

